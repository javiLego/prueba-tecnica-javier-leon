import { LitElement } from "lit-element";

export default class GetMyPostsDm extends LitElement {
  static get properties() {
    return {};
  }

  constructor() {
    super();
  }
  _handleSuccessResponse(data) {
    try {
      let detail = this._formatData(data);
      this.dispatchEvent(new CustomEvent("success-call", { detail }));
    } catch (e) {
      console.log(e);
      this._handleErrorResponse(data);
    }
  }

  _formatData(users) {
    //Destructuring
    return users.map(({ name, age }) => ({ name, age }));
  }

  _handleErrorResponse(dataErr) {
    let detail = {
      title: "Error de conexion",
      body: "Intentan de nuevio mas tarde",
    };
    this.dispatchEvent(new CustomEvent("error-call", { detail }));
  }

  generateRequest() {
    fetch("./components/sources/posts.json")
      .then((response) => response.json())
      .then((response) => {
        this._handleSuccessResponse(response);
      })
      .catch((err) => {
        this._handleErrorResponse(err);
      });
  }
}

customElements.define("get-my-posts", GetMyPostsDm);
